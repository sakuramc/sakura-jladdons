package ch.verttigo.jladdons;


import com.syntaxphoenix.jumpleagueplusapi.events.CheckPointEvent;
import com.syntaxphoenix.jumpleagueplusapi.events.DeathEvent;
import com.syntaxphoenix.jumpleagueplusapi.events.GameEndEvent;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class JLAddons extends JavaPlugin implements Listener {

    public static Economy economy;

    public static Economy getEconomy() {
        return JLAddons.economy;
    }


    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(this, this);
        this.getConfig().options().copyDefaults(true);
        if (!this.setupEconomy()) {
            System.out.println(String.format("[%s] - Disabled due to no Vault dependency found!", this.getDescription().getName()));
            this.getServer().getPluginManager().disablePlugin(this);
            return;
        }
        getConfig().set("Rewards.Winner", 0);
        getConfig().set("Rewards.PerKill", 0);
        getConfig().set("Rewards.Participation", 0);
        getConfig().set("Rewards.PerCheckPoint", 0);
        this.saveConfig();
    }

    private boolean setupEconomy() {
        final RegisteredServiceProvider<Economy> economyProvider = (RegisteredServiceProvider<Economy>) this.getServer().getServicesManager().getRegistration((Class) Economy.class);
        if (economyProvider != null) {
            JLAddons.economy = economyProvider.getProvider();
        }
        return JLAddons.economy != null;
    }

    @EventHandler
    public void onGameStart(DeathEvent e) {
        Player p = e.getPlayer();
        Player killer = e.getKiller();
        getEconomy().depositPlayer(p, getConfig().getInt("Rewards.Participation"));
        getEconomy().depositPlayer(killer, getConfig().getInt("Rewards.PerKill"));
    }

    @EventHandler
    public void onReachCheckpoint(CheckPointEvent e) {
        Player p = e.getPlayer();
        getEconomy().depositPlayer(p, getConfig().getInt("Rewards.PerCheckPoint"));
    }

    @EventHandler
    public void onGameEnd(GameEndEvent e) {
        Player winner = e.getWinner();
        getEconomy().depositPlayer(winner, getConfig().getInt("Rewards.Winner"));
    }
}
